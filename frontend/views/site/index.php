<?php

use frontend\widgets\CustomListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\Search\DishSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $ingredients array */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Выберите ингредиенты!</h2>
    </div>

    <?php echo $this->render('/dish/_search', ['model' => $searchModel, 'ingredients' => $ingredients]); ?>

    <?= CustomListView::widget([
        'dataProvider' => $dataProvider,
        'emptyText' => 'Ничего не найдено',
        'itemView' => function ($model, $key, $index, $widget) {
            return $model->name;
        },
        'layout' => '{items}',
        'options' => [
            'tag' => 'ul',
            'class' => 'list-group'
        ],
        'itemOptions' => [
            'tag' => 'li',
            'class' => 'list-group-item'
        ],
        'showError' => (count($searchModel->ingredientList) < 2) ? 'Выберите больше ингредиентов' : '',
    ]); ?>
</div>
