<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model frontend\models\Search\DishSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $ingredients array */
?>

<div class="dish-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ingredientList')
            ->widget(Select2::class, [
                'data' => $ingredients,
                'options' => ['placeholder' => 'Выберите ингредиенты ...'],
                'toggleAllSettings' => [
                    'selectLabel' => '',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'multiple' => true,
                    'maximumSelectionLength'=> 5,
            ]]); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>