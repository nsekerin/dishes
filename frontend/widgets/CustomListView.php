<?php

namespace frontend\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ListView;


class CustomListView extends ListView
{
    public $showError;

    public function run()
    {
        if (empty($this->showError)) {
            parent::run();

            return;
        } else {
            $content = $this->showError;
        }

        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'div');
        echo Html::tag($tag, $content, $options);
    }
}
