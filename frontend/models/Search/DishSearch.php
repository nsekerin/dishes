<?php

namespace frontend\models\Search;

use common\models\DishIngredient;
use common\models\Ingredient;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Dish;

/**
 * DishSearch represents the model behind the search form of `common\models\Dish`.
 */
class DishSearch extends Dish
{
    public $ingredientList;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['ingredientList'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dish::find()->alias('d');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->innerJoin(DishIngredient::tableName() . ' di', 'di.dish_id = d.id');
        $query->innerJoin(Ingredient::tableName() . ' i', 'di.ingredient_id = i.id');
        $query->andWhere(['in', 'i.id', $this->ingredientList ? $this->ingredientList : []]);
        $query->andWhere('d.id NOT IN (
            select di2.dish_id from dishes.ingredient i2 inner join dishes.dish_ingredient di2 on di2.ingredient_id = i2.id where i2.hide = 1
        )');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->groupBy(['d.id']);
        $queryClone = clone $query;

        if ($queryClone->having('COUNT(i.id) = :iCount', ['iCount' => count($this->ingredientList)])->count() <= 0) {
            $query->having('COUNT(i.id) >= 2');
            $query->orderBy('COUNT(i.id) DESC');
        } else {
            $query->having('COUNT(i.id) = :iCount', ['iCount' => count($this->ingredientList)]);
        }

        return $dataProvider;
    }
}
