<?php

use yii\db\Migration;

/**
 * Class m200212_154016_init_tables_dish_and_ingredient
 */
class m200212_154016_init_tables_dish_and_ingredient extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dish}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createTable('{{%ingredient}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'hide' => $this->boolean()->defaultValue(0),
        ]);

        $this->createTable('{{%dish_ingredient}}', [
            'id' => $this->primaryKey(),
            'dish_id' => $this->integer()->notNull(),
            'ingredient_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk_dish_ingredient_dish_id',
            'dish_ingredient',
            'dish_id',
            'dish',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_dish_ingredient_ingredient_id',
            'dish_ingredient',
            'ingredient_id',
            'ingredient',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_dish_ingredient_ingredient_id', 'dish_ingredient');
        $this->dropForeignKey('fk_dish_ingredient_dish_id', 'dish_ingredient');

        $this->dropTable('dish_ingredient');
        $this->dropTable('ingredient');
        $this->dropTable('dish');
    }
}
