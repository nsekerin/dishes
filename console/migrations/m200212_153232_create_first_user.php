<?php

use yii\db\Migration;

/**
 * Class m200212_153232_create_first_user
 */
class m200212_153232_create_first_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $date = new DateTime();

        $this->insert('{{%user}}', [
            'username' => 'admin',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash(12345),
            'email' => 'admin@test.ru',
            'status' => 10,
            'created_at' => $date->getTimestamp(),
            'updated_at' => $date->getTimestamp(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', 'username = "admin"');
    }
}
