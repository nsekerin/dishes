<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ingredient".
 *
 * @property int $id
 * @property string $name
 * @property int|null $hide
 *
 * @property DishIngredient[] $dishIngredients
 */
class Ingredient extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['hide'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'hide' => 'Скрыть',
        ];
    }

    /**
     * Gets query for [[DishIngredients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDishIngredients()
    {
        return $this->hasMany(DishIngredient::className(), ['ingredient_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getAllForDropdownList()
    {
        $result = [];
        $ingredients = self::find()->all();

        /** @var Ingredient $ingredient */
        foreach ($ingredients as $ingredient) {
            $result[$ingredient->id] = $ingredient->name;
        }

        return $result;
    }

    /**
     * @return array
     */
    public static function getAllForDropdownListExceptHidden()
    {
        $result = [];
        $ingredients = self::find()->where(['hide' => false])->all();

        /** @var Ingredient $ingredient */
        foreach ($ingredients as $ingredient) {
            $result[$ingredient->id] = $ingredient->name;
        }

        return $result;
    }
}
