<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "dish".
 *
 * @property int $id
 * @property string $name
 *
 * @property DishIngredient[] $dishIngredients
 */
class Dish extends ActiveRecord
{
    public $ingredientList;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dish';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['ingredientList'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'ingredientList' => 'Ингредиенты'
        ];
    }

    public function behaviors()
    {
        return ['ingredientList' => [
            'class' => AttributeBehavior::class,
            'attributes' => [
                ActiveRecord::EVENT_AFTER_FIND => 'ingredientList'
            ],
            'value' => function ($event) {
                $result = [];
                $ingredients = $this->getIngredients()->all();

                /** @var Ingredient $ingredient */
                foreach ($ingredients as $ingredient) {
                    $result[$ingredient->id] = $ingredient->id;
                }

                return $result;
            },
        ]];
    }

    /**
     * Gets query for [[DishIngredients]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDishIngredients()
    {
        return $this->hasMany(DishIngredient::className(), ['dish_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getIngredients()
    {
        return $this->hasMany(Ingredient::className(), ['id' => 'ingredient_id'])
            ->viaTable('dish_ingredient', ['dish_id' => 'id']);
    }

    /**
     * @return string\
     */
    public function getIngredientsNames()
    {
        $result = '';

        /** @var Ingredient $ingredient */
        foreach ($this->ingredients as $ingredient) {
            $result .= $ingredient->name . ', ';
        }

        $result = trim($result, ', ');

        return $result;
    }
}
